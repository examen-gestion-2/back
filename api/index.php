
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . "/Core/Inc/Bootstrap.php";

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: DELETE, POST, GET, PUT, OPTIONS");

$controllerHelper = new ControllerHelper();

// Explode the uri to obtain an array
$uri = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
$uri = explode("/", $uri);

// Extract the data from the uri if exists
$GLOBALS['uriController'] = isset($uri[3]) ? ucfirst($uri[3]) : null;
$GLOBALS['uriAction'] = isset($uri[4]) ? strtolower($uri[4]) : null;
$GLOBALS['uriId'] = isset($uri[5]) ? $uri[5] : null;

// Check if a controller is requested
if (!$GLOBALS['uriController']) {
    header("HTTP/1.1 404 Not Found");
    echo json_encode(array("error" => "404 Controller not requested"));
    exit();
}



// Check if the requested controller exists
if (!$controllerHelper->existsController($GLOBALS['uriController'])) {
    header("HTTP/1.1 404 Not Found");
    echo json_encode(array("error" => "404 Controller not found"));
    exit();
}

// Create the dinamic controller
require PROJECT_ROOT_PATH . "/Controllers/" . $GLOBALS['uriController'] . "Controller.php";
$strControllerName = $GLOBALS['uriController'] . "Controller";
$objFeedController = new $strControllerName();

// Check if the action is a rest id
if (is_numeric($GLOBALS["uriAction"])) {
    $GLOBALS["uriId"] = $GLOBALS["uriAction"];
    $GLOBALS["uriAction"] = "";
}

// Check if the requested action exists
if ( ($GLOBALS["uriAction"] && strcasecmp($GLOBALS["uriAction"], "rest") == 0) || ($GLOBALS["uriAction"] && !method_exists($objFeedController, $GLOBALS["uriAction"]))) {
    header("HTTP/1.1 404 Not Found");
    echo json_encode(array("error" => "404 Action not found"));
    exit();
}

// If there is no action access to the rest method
if (!$GLOBALS["uriAction"]) {
    $GLOBALS["uriAction"] = "rest";
}

// If there is an action, then access to the action
$strMethodName = $GLOBALS["uriAction"];
$objFeedController->{$strMethodName}();