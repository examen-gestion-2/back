<?php


class StockController extends RestController
{

  public function __construct()
  {
    parent::__construct("Stock");
  }

  public function deplete()
  {
      if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS'){
          $responseData = json_encode(array("success" => "Pass the options"));
      }else if ($_SERVER['REQUEST_METHOD'] != 'POST' || !$GLOBALS["uriId"]) {
          http_response_code(405);
          echo json_encode(['status' => 'error', 'message' => 'Method Not Allowed']);
          return;
      }

      $strError = "";
      $result = $this->service->depleteOne();

      if ($result > 0) {
          $responseData = json_encode(array("success" => "Stock was depleted."));
      } else {
          $strError = "Stock was not updated!." . " Please contact support.";
          $strErrorHeader = "HTTP/1.1 500 Internal Server Error";
      }

      if (!$strError) {
          $this->sendOutput(
              $responseData,
              array("Content-Type: application/json", "HTTP/1.1 200 OK")
          );
      } else {
          $this->sendOutput(
              json_encode(array("error" => $strError)),
              array("Content-Type: application/json", $strErrorHeader)
          );
      }
  }
}