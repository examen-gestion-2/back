<?php
class UserController extends RestController
{

    private $userModel;

    public function __construct()
    {
        parent::__construct("User");
        $this->userModel = new UserModel();
    }

    public function getOne($id)
    {
        /**
         * Retrieve specific user using the specified id, Encode the user as a JSON object.
         * Set response headers. Send the encoded output as an HTTP response
         */
        $this->sendOutput(json_encode($this->userModel->getOne($id)),
           array('Content-Type: application/json',"Product")
         );
    }

    public function updateOne($id, $userName, $password, $rol){
        // Call updateOne method from userModel and pass in the provided id, username, password and role
        // Encode the returned value as json
        $json = json_encode($this->userModel->updateOne($id, $userName, $password, $rol));
        // Send the json encoded value to the client with specified Content-Type header
        $this->sendOutput($json, array('Content-Type: application/json',"Product"));
    }
    
    public function verify($userName, $password)
    {
        // Call verifyOne method from userModel and pass in the provided username and password
        // Encode the returned value as json
        $json = json_encode($this->userModel->verifyOne($userName, $password));
        // Send the json encoded value to the client with specified Content-Type header
        $this->sendOutput($json, array('Content-Type: application/json',"Product"));
    }

    public function login()
    {
        //Validate request method
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            http_response_code(405); 
            echo json_encode(['status' => 'error', 'message' => 'Method Not Allowed']);
            return;
        }
        
        try {
            //Call verifyOne method to verify the user
            $verificationResult = $this->service->verifyOne();
            
            //Check if verification result is empty or if 'rol' index is not set
            if(empty($verificationResult) || !isset($verificationResult[0]['rol'])){
                //Return bad request error if the data is invalid
                http_response_code(400); 
                echo json_encode(['status' => 'error', 'message' => 'Invalid user data']);
                return;
            }
            //Return success status with rol as a parameter
            http_response_code(200); 
            echo json_encode(['status' => 'success', 'rol' => $verificationResult[0]['rol']]);
        } catch (\Exception $e) {
            //Return internal server error in case of exception
            http_response_code(500);
            echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }
}
