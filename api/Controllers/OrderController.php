<?php


class OrderController extends RestController
{

    public function __construct()
    {
        parent::__construct("Order");
    }

    public function state_change()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS'){
            $responseData = json_encode(array("success" => "Pass the options"));
        }else if ($_SERVER['REQUEST_METHOD'] != 'POST' || !$GLOBALS["uriId"]) {
            http_response_code(405);
            echo json_encode(['status' => 'error', 'message' => 'Method Not Allowed']);
            return;
        }

        $strError = "";
        $result = $this->service->changeState();

        if ($result > 0) {
            $responseData = json_encode(array("success" => "State was updated."));
        } else {
            $strError = "State was not updated!." . " Please contact support.";
            $strErrorHeader = "HTTP/1.1 500 Internal Server Error";
        }

        if (!$strError) {
            $this->sendOutput(
                $responseData,
                array("Content-Type: application/json", "HTTP/1.1 200 OK")
            );
        } else {
            $this->sendOutput(
                json_encode(array("error" => $strError)),
                array("Content-Type: application/json", $strErrorHeader)
            );
        }
    }

    public function today()
    {
  
      $strErrorHeader = "";
      if ($_SERVER['REQUEST_METHOD'] != 'GET' ) {
        http_response_code(405);
        echo json_encode(['status' => 'error', 'message' => 'Method Not Allowed']);
        return;
      }
  
      $strError = "";
      $arr = $this->service->getToday();

      
      $responseData = json_encode($arr);
  
      

      if (!$strError) {
        $this->sendOutput(
          $responseData,
          array("Content-Type: text/json", "HTTP/1.1 200 OK")
        );
      } else {
        $this->sendOutput(
          json_encode(array("error" => $strError)),
          array("Content-Type: application/json", $strErrorHeader)
        );
      }
    }
}
