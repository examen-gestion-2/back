<?php


class ProductController extends RestController
{

  public function __construct()
  {
    parent::__construct("Product");
  }

  public function imageless()
  {
    //check if the request method is not GET or if the global "uriId" variable is set.
    if ($_SERVER['REQUEST_METHOD'] != 'GET' || $GLOBALS["uriId"]) {
      http_response_code(405);
      echo json_encode(['status' => 'error', 'message' => 'Method Not Allowed']);
      return;
    }

    $strError = "";
    //use the getAllImageLess method of the service object to retrieve data
    $arr = $this->service->getAllImageLess();
    $responseData = json_encode($arr);

    if (count($arr) > 0) {
      $responseData = json_encode($arr);
    } else {
      $strError = "Data not found!.";
      $strErrorHeader = "HTTP/1.1 404 Data not found";
    }

    //Check if there is no error
    if (!$strError) {
      //send the data with headers "Content-Type: application/json" and "HTTP/1.1 200 OK"
      $this->sendOutput(
        $responseData,
        array("Content-Type: application/json", "HTTP/1.1 200 OK")
      );
    } else {
      //send the error message with headers "Content-Type: application/json" and the error header
      $this->sendOutput(
        json_encode(array("error" => $strError)),
        array("Content-Type: application/json", $strErrorHeader)
      );
    }
  }

  public function image()
  {
    // check if the request method is not GET or if the global "uriId" variable is not set.
    if ($_SERVER['REQUEST_METHOD'] != 'GET' || !$GLOBALS["uriId"]) {
      http_response_code(405);
      echo json_encode(['status' => 'error', 'message' => 'Method Not Allowed']);
      return;
    }

    $strError = "";
    // use the getImage method of the service object to retrieve data
    $arr = $this->service->getImage();
    $responseData = json_encode($arr);

    // check if data was found
    if (count($arr) > 0) {
      // assign the value of the 'image' key of the first element of the $arr to $responseData
      $responseData = $arr[0]['image'];
    } else {
      // set error message if data not found
      $strError = "Data not found!.";
      $strErrorHeader = "HTTP/1.1 404 Data not found";
    }

    // check if there is no error
    if (!$strError) {
      // send the data with headers "Content-Type: text/plain" and "HTTP/1.1 200 OK"
      $this->sendOutput(
        $responseData,
        array("Content-Type: text/plain", "HTTP/1.1 200 OK")
      );
    } else {
      // send the error message with headers "Content-Type: application/json" and the error header
      $this->sendOutput(
        json_encode(array("error" => $strError)),
        array("Content-Type: application/json", $strErrorHeader)
      );
    }
  }
}
