<?php


class BaseService
{
    protected function exitOutput($data, $httpHeaders = array())
    {
        header_remove('Set-Cookie');
        if (is_array($httpHeaders) && count($httpHeaders)) {
            foreach ($httpHeaders as $httpHeader) {
                header($httpHeader);
            }
        }
        header("Content-Type: application/json");
        echo $data;
        exit;
    }
}