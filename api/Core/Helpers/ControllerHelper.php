 
<?php

class ControllerHelper
{
    private $controllers;

    public function __construct()
    {
        $this->controllers = $this->getControllers();
    }

    /**
     * Check for controller names in controllers folder
     */
    private function getControllers(){
        $arrFiles = scandir('./Controllers');
        $controllers = array();
        foreach ($arrFiles as $_ => $file) {
 
            if (str_contains($file, "Controller")){
              
                array_push($controllers,$file) ;
                //Buscar como insertar datos en este array
            }
        } 
        return $controllers;
    }

    /**
     * Check if the controller exists
     */
    public function existsController($requestedController){

        foreach ($this->controllers as $_ => $controller) {
          
            if (strcasecmp($requestedController."Controller.php", $controller) == 0){
               return true;
            }
          
        }  
        return false;
    }
}