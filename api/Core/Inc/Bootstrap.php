<?php
define("PROJECT_ROOT_PATH", __DIR__ . "/../..");

// Global
require_once PROJECT_ROOT_PATH . "/Core/Inc/Config.php";
require_once PROJECT_ROOT_PATH . "/Core/Inc/Constants.php";

// ------------------------------------------------------------

// Core

require_once PROJECT_ROOT_PATH . "/Core/RestController.php";
require_once PROJECT_ROOT_PATH . "/Core/BaseModel.php";
require_once PROJECT_ROOT_PATH . "/Core/BaseService.php";

// ------------------------------------------------------------

// Helpers

require_once PROJECT_ROOT_PATH . "/Core/Helpers/ControllerHelper.php";

// ------------------------------------------------------------

// Models

require_once PROJECT_ROOT_PATH . "/Model/ClientModel.php";
require_once PROJECT_ROOT_PATH . "/Model/ProductModel.php";
require_once PROJECT_ROOT_PATH . "/Model/UserModel.php";
require_once PROJECT_ROOT_PATH . "/Model/StockModel.php";
require_once PROJECT_ROOT_PATH . "/Model/OrderModel.php";
// ------------------------------------------------------------

