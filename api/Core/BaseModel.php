<?php
require_once PROJECT_ROOT_PATH . "/Database/Database.php";

class BaseModel
{
    private $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * Used on select queries.
     * Returns: Result of the selection.
     */
    public function select($query = "" , $params = [])
    {

        try {
            $stmt = $this->database->executeStatement( $query , $params );
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);				
            $stmt->close();
            return $result;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }
        return false;
    }

    /**
     * Used on update queries.
     * Returns: Number of affected rows.
     */
    public function update($query = "" , $params = [])
    {

        try {
            $stmt = $this->database->executeStatement( $query , $params );
            $result = $stmt->affected_rows;				
            $stmt->close();
            return $result;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }
        return false;
    }

}