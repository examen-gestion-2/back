<?php
class RestController
{
    protected $service;

    public function __construct($serviceName)
    {
        // Create the service dynamically based on the name
        require PROJECT_ROOT_PATH . "/Service/" . $serviceName . "Service.php";
        $strService = $serviceName . "service";
        $this->service = new $strService();
    }

    public function __call($name, $arguments)
    {
        $this->sendOutput('', array('HTTP/1.1 404 Not Found'));
    }


    /**
     * All the rest controllers will have a base rest action.
     */
    public function rest()

    {
        $strError = "";
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        $responseData = [];

        try {
            switch (strtoupper($requestMethod)) {
                case OPTIONS:
                    $responseData = json_encode(array("success" => "Pass the options"));
                    break;
                case GET:

                    if (isset($GLOBALS["uriId"])) {

                        $this->validateId();
                        $arr = $this->service->getOne();

                        $responseData = json_encode($arr);

                        if(count($arr) > 0){
                            $responseData = json_encode($arr[0]);
                        }else {
                            $strError = "Data not found!.";
                            $strErrorHeader = "HTTP/1.1 404 Data not found";
                        }

                    } else {
                        $arr = $this->service->getAll();
                        
                        if (count($arr) == 0) {
                            //Return no data error
                            http_response_code(204); 
                            echo json_encode(['status' => 'error', 'message' => 'No data found']);
                            return;
                        }
                        $responseData = json_encode($arr);
                    }

                    break;

                case POST:
                    $result = $this->service->postOne();

                    if ($result > 0) {
                        $responseData = json_encode(array("success" => "Data was posted."));
                    } else {
                        $strError = "Data was not posted!." . " Please contact support.";
                        $strErrorHeader = "HTTP/1.1 500 Internal Server Error";
                    }

                    break;

                case PUT:
                    $this->validateId();
                    $result = $this->service->updateOne();
                    if ($result > 0) {
                        $responseData = json_encode(array("success" => "Data was updated."));
                    } else {
                        $strError = "Data was not updated!." . " Please contact support.";
                        $strErrorHeader = "HTTP/1.1 500 Internal Server Error";
                    }

                    break;

                case DELETE:
                    $this->validateId();
                    $result = $this->service->deleteOne();
                    if ($result > 0) {
                        $responseData = json_encode(array("success" => "Data was deleated."));
                    } else {
                        $strError = "Id not found!." . "";
                        $strErrorHeader = "HTTP/1.1 404 Not Found";
                    }

                    break;

                default:
                    $strError = "Method not supported";
                    $strErrorHeader = "HTTP/1.1 422 Unprocessable Entity";
                    break;
            }
        } catch (Error $e) {
            $strError = $e->getMessage() . "Something went wrong! Please contact support.";
            $strErrorHeader = "HTTP/1.1 500 Internal Server Error";
        }


        if (!$strError) {
            $this->sendOutput(
                $responseData,
                array("Content-Type: application/json", "HTTP/1.1 200 OK")
            );
        } else {
            $this->sendOutput(
                json_encode(array("error" => $strError)),
                array("Content-Type: application/json", $strErrorHeader)
            );
        }
    }

    protected function getUriSegments()
    {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = explode('/', $uri);
        return $uri;
    }

    protected function sendOutput($data, $httpHeaders = array())
    {
        header_remove('Set-Cookie');
        if (is_array($httpHeaders) && count($httpHeaders)) {
            foreach ($httpHeaders as $httpHeader) {
                header($httpHeader);
            }
        }
        echo $data;
        exit;
    }

    protected function validateId()
    {
        $id = isset($GLOBALS["uriId"]) ? $GLOBALS["uriId"] : null;
        if (!is_numeric($id) || $id <  INT_MIN || $id > INT_MAX) {
            //Return bad request error if the data is invalid
            $this->sendOutput(json_encode(['status' => 'error', 'message' => 'Invalid Id']), array("Content-Type: application/json", "HTTP/1.1 400 Invalid Id"));
        }
    }
}
