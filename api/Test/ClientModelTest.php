<?php

use PHPUnit\Framework\TestCase;

define('PROJECT_ROOT_PATH', realpath(dirname(__FILE__) . '/..'));

final class ClientModelTest extends TestCase
{
    private $clientModel;

    public function setUp(): void
    {
        $this->clientModel = new ClientModel();
    }
 
    public function testGetOneSuccess()
    {
        // Arrange
        $id = 26;
        $name = "Adam 2925e";
        // Act
        $result = $this->clientModel->getOne($id);

        //Assert
        $this->assertEquals($id, $result[0]['clientId']);
        $this->assertEquals($name, $result[0]['name']);
    }

    public function testGetOneInvalidId()
    {
        // Arrange
        $id = -13;
        // Act
        $result = $this->clientModel->getOne($id);

        $this->assertEmpty($result, "Expected an empty array.");
    }

    public function testPostOneSuccess()
    {
        // Arrange
        $name = 'John Doe';
        $phone = '84313333';
        $address = '123 Main St.';
        $registerDate = '2023-02-01';
        $expectedResult = 1;

        // Act
        $result = $this->clientModel->postOne($name, $phone, $address, $registerDate);

        $this->assertEquals($expectedResult, $result);
        //consltar el nuve id
    }

    public function testUpdateOneSuccess()
    {
        // Arrange
        $id = 14;
        $name = 'John Doe '.$this->clientModel->getALL()[0]['clientId'];;
        $phone = '84313338';
        $address = '123 Main St.';
        $registerDate = '2022-01-01';
        $expectedResult = 1;

        // Act
        $result = $this->clientModel->updateOne($id, $name, $phone, $address, $registerDate);

        $this->assertEquals($expectedResult, $result);

        $result = $this->clientModel->getOne($id);
        $this->assertEquals($name, $result[0]['name']);
    }

    public function testDeleteOneSuccess()
    {
        // Insert a record into the database
        $id = $this->clientModel->getALL()[0]['clientId'];
        // Call the deleteOne function
        $result = $this->clientModel->deleteOne($id);
        // Assert that the function returns true
        // Assume you have received the response in the $response variable
        $expectedResult = 1;

        // Check if the values of the keys are as expected
        $this->assertEquals($expectedResult, $result);
        // (Assuming you have a method to retrieve records from the database)
                
        //$result = $this->clientModel->getOne($id);

        ///$this->assertEmpty($result, "Expected an empty array.");

    }

    public function testDeleteOneInvalidId()
    {
        // Insert a record into the database
        $id = -11;
        // Call the deleteOne function
        $result = $this->clientModel->deleteOne($id);
        // Assert that the function returns true
        // Assume you have received the response in the $response variable
        $expectedResult = 0;

        // Check if the values of the keys are as expected
        $this->assertEquals($expectedResult, $result);
    }
}

