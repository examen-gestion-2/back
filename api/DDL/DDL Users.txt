
CREATE TABLE IF NOT EXISTS `Users` (
  `userId` bigint NOT NULL AUTO_INCREMENT,
  `userName` varchar(25) NOT NULL,
  `password` varchar(75) NOT NULL,
  `rol` int NOT NULL,
  `isActive` bit NOT NULL DEFAULT true,
  PRIMARY KEY (`userId`)
);



INSERT INTO UCR_Gestion.Users
(userName, password, `rol`)
VALUES('Admin', '123123', 1);


#------------------------------------------------

#Get All users

DROP PROCEDURE IF EXISTS sp_users_getAll;

DELIMITER //
CREATE PROCEDURE sp_users_getAll()
BEGIN
	SELECT userId, userName, password, rol
	FROM Users
	WHERE isActive = TRUE;
END //

DELIMITER ;

#------------------------------------------------

#Get One user

DROP PROCEDURE IF EXISTS sp_users_getOne;

DELIMITER //
CREATE PROCEDURE sp_users_getOne(IN $userId INT)
BEGIN
	SELECT userId, userName, password, rol
	FROM Users 
	WHERE userId = $userId and isActive = TRUE;

END //

DELIMITER ;

#------------------------------------------------

#Post One user

DROP PROCEDURE IF EXISTS sp_users_postOne;

DELIMITER //
CREATE PROCEDURE sp_users_postOne(IN $userName VARCHAR(25),IN $password VARCHAR(75),IN $rol INT)
	BEGIN
	INSERT INTO UCR_Gestion.Users (userName, password, rol)
	VALUES($userName, $password, $rol);
END //

DELIMITER ;


#------------------------------------------------

#Update One user

DROP PROCEDURE IF EXISTS sp_users_updateOne;

DELIMITER //
CREATE PROCEDURE sp_users_updateOne(IN $userId int, IN $userName varchar(25),IN $password varchar(75),IN $rol int)
BEGIN

	UPDATE Users
	SET userName = $userName, password=$password, rol= $rol
	WHERE userId = $userId and isActive = TRUE;

END //

DELIMITER ;

#------------------------------------------------

#Delete One user

DROP PROCEDURE IF EXISTS sp_users_deleteOne;

DELIMITER //
CREATE PROCEDURE sp_users_deleteOne(IN $userId INT)
BEGIN

	UPDATE Users
	SET isActive = false
	WHERE userId = $userId and isActive = TRUE;

END //

DELIMITER ;

#--------------------------------------------------

#Verify user

DROP PROCEDURE IF EXISTS sp_users_verifyOne;

DELIMITER //
CREATE PROCEDURE sp_users_verifyOne(IN $userName VARCHAR(25), IN $password VARCHAR(255))
BEGIN
    SELECT rol
    FROM Users
    WHERE userName = $userName AND password = $password AND isActive = TRUE;
END //

DELIMITER ;