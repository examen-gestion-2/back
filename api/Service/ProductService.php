<?php
class ProductService extends BaseService
{

    private $productModel;
    private $jsonData;

    public function __construct()
    {
        $this->productModel = new ProductModel();
        $this->jsonData = json_decode(file_get_contents('php://input'));
    }

    public function getAll()
    {
        // Retrieve all products from the productModel object
        return $this->productModel->getAll();
    }

    public function getAllImageLess()
    {
        // Retrieve all products without image from the productModel object
        return $this->productModel->getAllImageLess();
    }

    public function getImage(){

        // Retrieve the id from the global uriId variable
        $id = $GLOBALS["uriId"];
        return $this->productModel->getImage($id);
    }

    public function getOne()
    {
        // Retrieve the id from the global uriId variable
        $id = $GLOBALS["uriId"];
        // Retrieve a single product based on the id from the productModel object
        return $this->productModel->getOne($id);
    }

    public function postOne()
    {
        // Retrieve data from the request using obtainData method
        $data = $this->obtainData();
        // Insert a new product with the data obtained from the request using the productModel object
        return $this->productModel->postOne(...$data);
    }

    public function updateOne()
    {
        // Retrieve the id from the global uriId variable
        $id = $GLOBALS["uriId"];
        // Retrieve data from the request using obtainData method
        $data = $this->obtainData();

        // Update a product with the data obtained from the request and the id using the productModel object
        return $this->productModel->updateOne($id, ...$data);
    }

    public function deleteOne()
    {
        // Retrieve the id from the global uriId variable
        $id = $GLOBALS["uriId"];
        // Delete a product based on the id using the productModel object
        return $this->productModel->deleteOne($id);
    }

    public function obtainData()
    {
        // Retrieve the name, description, price, category, ingredients, image from the jsonData object, if it's missing send an error response with HTTP/1.1 400 Bad Request status
        $name = isset($this->jsonData->name) ? $this->jsonData->name : $this->exitOutput(json_encode(array("error" => "Missing name.")), array('HTTP/1.1 400 Bad Request'));
        $description = isset($this->jsonData->description) ? $this->jsonData->description : $this->exitOutput(json_encode(array("error" => "Missing description.")), array('HTTP/1.1 400 Bad Request'));
        $price = isset($this->jsonData->price) ? $this->jsonData->price : $this->exitOutput(json_encode(array("error" => "Missing price.")), array('HTTP/1.1 400 Bad Request'));
        $category = isset($this->jsonData->category) ? $this->jsonData->category : $this->exitOutput(json_encode(array("error" => "Missing category.")), array('HTTP/1.1 400 Bad Request'));
        $ingredients  = isset($this->jsonData->ingredients) ? $this->jsonData->ingredients : $this->exitOutput(json_encode(array("error" => "Missing ingredients.")), array('HTTP/1.1 400 Bad Request'));
        $image = isset($this->jsonData->image) ? $this->jsonData->image : $this->exitOutput(json_encode(array("error" => "Missing image.")), array('HTTP/1.1 400 Bad Request'));
        $discount = isset($this->jsonData->discount) ? $this->jsonData->discount : $this->exitOutput(json_encode(array("error" => "Missing discount.")), array('HTTP/1.1 400 Bad Request'));

        $data = array("name" => $name, "description" => $description, "price" => $price, "category" => $category, "ingredients" => $ingredients, "image" => $image, "discount" => $discount);

        // Validate the data using the validateData method
        $this->validateData($data);
        
        // Return the data array
        return $data;
    }

    private function validateData($data)
    {

        // Type validations
        if (!is_string($data["name"])) {
            $this->exitOnError("Name must be a string");
        }

        if (!is_string($data["description"])) {
            $this->exitOnError("Description must be a string");
        }

        if (!is_numeric($data["price"])) {
            $this->exitOnError("Price must be a string");
        }

        if (!is_string($data["category"])) {
            $this->exitOnError("Category date must be a string");
        }

        if (!is_string($data["ingredients"])) {
            $this->exitOnError("Ingredients must be a string");
        }

        if (!is_string($data["image"])) {
            $this->exitOnError("Image must be a string");
        }

        if (!is_numeric($data["discount"])) {
            $this->exitOnError("Discount date must be a string");
        }


        // Extra validations
        if (strlen($data["name"]) < 5) {
            $this->exitOnError("Name must have 5 or more characters");
        }

        if (strlen($data["name"]) > 35) {
            $this->exitOnError("Exeeded name length limit 35");
        }

        if (strlen($data["description"]) < 5) {
            $this->exitOnError("Description must have 5 or more characters");
        }

        if (strlen($data["category"]) > 20) {
            $this->exitOnError("Exeeded category length limit 20");
        }

        if (strlen($data["category"]) < 5) {
            $this->exitOnError("Category must have 5 or more characters");
        }

        if (strlen($data["description"]) > 75) {
            $this->exitOnError("Exeeded description length limit 75");
        }

        if ($data["price"] < 0 || $data["price"] > 75000) {
            $this->exitOnError("Price must be a number between 0 and 75000");
        }

        if ($data["discount"] < 0 || $data["discount"] > 100) {
            $this->exitOnError("Discount must be a number between 0 and 100");
        }

        // Check if the image property starts with 'data:image/'
        if(strpos($data["image"], 'data:image/')==0){
            $this->exitOutput(json_encode(array("error" => "Exeeded image limit.")), array('HTTP/1.1 400 Bad Request'));
        }
        
    }

    private function exitOnError($message)
    {
        $this->exitOutput(json_encode(array("error" => $message)), array('HTTP/1.1 400 Bad Request'));
    }

}
