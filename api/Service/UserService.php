<?php
class UserService
{

    private $userModel;
    private $jsonData;

    public function __construct()
    {
        // Initialize an instance of the UserModel class
        $this->userModel = new UserModel();
        // Decode the request body as JSON and assign it to jsonData property
        $this->jsonData = json_decode(file_get_contents('php://input'));
    }

    public function getOne()
    {
        // Retrieve the id from the global uriId variable
        $id = $GLOBALS["uriId"];
        // Use the id to retrieve a user from the userModel object
        return $this->userModel->getOne($id);
    }

    public function updateOne()
    {
        // Retrieve the id from the global uriId variable
        $id = $GLOBALS["uriId"];

        // Retrieve the values of userName, password and rol from the jsonData properties
        $userName = isset($this->jsonData->userName) ? $this->jsonData->userName : null;
        $password = isset($this->jsonData->password) ? $this->jsonData->password : null;  
        $rol  = isset($this->jsonData->rol) ? $this->jsonData->rol : null;     

        // Use the id and the retrieved values to update a user in the userModel object
        return $this->userModel->updateOne($id, $userName, $password, $rol);
    }

    public function verifyOne()
    {
        // Retrieve the values of userName and password from the jsonData properties
        $userName = isset($this->jsonData->userName) ? $this->jsonData->userName : null;
        $password = isset($this->jsonData->password) ? $this->jsonData->password : null;      

        // Use the retrieved values to verify a user in the userModel object
        return $this->userModel->verifyOne($userName, $password);
    }
}
