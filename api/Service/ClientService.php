<?php
class ClientService extends BaseService
{

    private $clientModel;
    private $jsonData;

    public function __construct()
    {
        $this->clientModel = new ClientModel();
        $this->jsonData = json_decode(file_get_contents('php://input'));
    }

    public function getAll()
    {
        return $this->clientModel->getAll();
    }

    public function getOne()
    {
        $id = $GLOBALS["uriId"];
        return $this->clientModel->getOne($id);
    }

    public function postOne()
    {
        $data = $this->obtainData();
        return $this->clientModel->postOne(...$data);
    }

    public function updateOne()
    {
        $id = $GLOBALS["uriId"];
        $data = $this->obtainData();
        return $this->clientModel->updateOne($id, ...$data);
    }

    public function deleteOne()
    {
        $id = $GLOBALS["uriId"];
        return $this->clientModel->deleteOne($id);
    }

    // Return an array with validated data.
    private function obtainData()
    {
        $name = isset($this->jsonData->name) ? $this->jsonData->name : $this->exitOutput(json_encode(array("error" => "Missing name.")), array('HTTP/1.1 400 Bad Request'));
        $phone = isset($this->jsonData->phone) ? $this->jsonData->phone : $this->exitOutput(json_encode(array("error" => "Missing phone.")), array('HTTP/1.1 400 Bad Request'));
        $address = isset($this->jsonData->address) ? $this->jsonData->address : $this->exitOutput(json_encode(array("error" => "Missing address.")), array('HTTP/1.1 400 Bad Request'));
        $registerDate = isset($this->jsonData->registerDate) ? $this->jsonData->registerDate : $this->exitOutput(json_encode(array("error" => "Missing registerDate.")), array('HTTP/1.1 400 Bad Request'));

        $data = array("name" => $name, "phone" => $phone, "address" => $address, "registerDate" => $registerDate);

        $this->validateData($data);

        return $data;
    }

    private function validateData($data)
    {

        // Type validations
        if (!is_string($data["name"])) {
            $this->exitOnError("Name must be a string");
        }

        if (!is_string($data["phone"])) {
            $this->exitOnError("Phone must be a string");
        }

        if (!is_string($data["address"])) {
            $this->exitOnError("Address must be a string");
        }

        if (!is_string($data["registerDate"])) {
            $this->exitOnError("Register date must be a string");
        }

        // Extra validations
        if (strlen($data["name"]) > 35) {
            $this->exitOnError("Exeeded name length limit");
        }

        if (strlen($data["name"]) < 2) {
            $this->exitOnError("Too short name");
        }

        if (strlen($data["phone"]) !== 8) {
            $this->exitOnError("Error on phone length");
        }
        
        if (!is_numeric($data["phone"])) {
            $this->exitOnError("Phone can't contain letters");
        }

        if (strlen($data["address"]) > 200) {
            $this->exitOnError("Exeeded address length limit");
        }

        if (strlen($data["address"]) < 5) {
            $this->exitOnError("Too short address");
        }

        if (!$this->validateDate($data["registerDate"])) {
            $this->exitOnError("Invalid date format");
        }
    }

    private function exitOnError($message){
        $this->exitOutput(json_encode(array("error" => $message)), array('HTTP/1.1 400 Bad Request'));
    }

    private function validateDate($date)
    {
        $dateTime = DateTime::createFromFormat('Y-m-d', $date);
        return $dateTime && $dateTime->format('Y-m-d') === $date;
    }
}