<?php
class StockService extends BaseService
{

    private $stockModel;
    private $jsonData;

    public function __construct()
    {
        $this->stockModel = new StockModel();
        $this->jsonData = json_decode(file_get_contents('php://input'));
    }

    public function getAll()
    {
        return $this->stockModel->getAll();
    }

    public function getOne()
    {
        $id = $GLOBALS["uriId"];
        return $this->stockModel->getOne($id);
    }

    public function postOne()
    {
        $data = $this->obtainData();
        return $this->stockModel->postOne(...$data);
    }

    public function updateOne()
    {
        $id = $GLOBALS["uriId"];
        $data = $this->obtainData();
        return $this->stockModel->updateOne($id, ...$data);
    }

    public function deleteOne()
    {
        $id = $GLOBALS["uriId"];
        return $this->stockModel->deleteOne($id);
    }

    public function depleteOne()
    {
        $id = $GLOBALS["uriId"];
        return $this->stockModel->depleteOne($id);
    }

    // Return an array with validated data.
    private function obtainData()
    {
        $name = isset($this->jsonData->name) ? $this->jsonData->name : $this->exitOutput(json_encode(array("error" => "Missing name.")), array('HTTP/1.1 400 Bad Request'));
        $price = isset($this->jsonData->price) ? $this->jsonData->price : $this->exitOutput(json_encode(array("error" => "Missing price.")), array('HTTP/1.1 400 Bad Request'));
        $quantity = isset($this->jsonData->quantity) ? $this->jsonData->quantity : $this->exitOutput(json_encode(array("error" => "Missing quantity.")), array('HTTP/1.1 400 Bad Request'));
        $registerDate = isset($this->jsonData->registerDate) ? $this->jsonData->registerDate : $this->exitOutput(json_encode(array("error" => "Missing registerDate.")), array('HTTP/1.1 400 Bad Request'));

        $data = array("name" => $name, "price" => $price, "quantity" => $quantity, "registerDate" => $registerDate);

        $this->validateData($data);

        return $data;
    }

    private function validateData($data)
    {

        // Type validations
        if (!is_string($data["name"])) {
            $this->exitOnError("Name must be a string");
        }

        if (!is_numeric($data["price"])) {
            $this->exitOnError("Price must be numeric");
        }

        if (!is_numeric($data["quantity"])) {
            $this->exitOnError("Quantity must be numeric");
        }

        if (!is_string($data["registerDate"])) {
            $this->exitOnError("Register date must be a string");
        }

        // Extra validations
        if (strlen($data["name"]) > 35) {
            $this->exitOnError("Exeeded name length limit");
        }

        if (strlen($data["name"]) < 2) {
            $this->exitOnError("Too short name");
        }

        if ($data["price"] < 0) {
            $this->exitOnError("Price must be positive or zero");
        }

        if ($data["quantity"] < 0 ) {
            $this->exitOnError("Quantity must be positive or zero");
        }

        if ($data["price"] > 100000) {
            $this->exitOnError("Price must be lower than 100000");
        }

        if ($data["quantity"] > 10000 ) {
            $this->exitOnError("Quantity must be lower than 10000");
        }

        if (!$this->validateDate($data["registerDate"])) {
            $this->exitOnError("Invalid date format");
        }
    }

    private function exitOnError($message){
        $this->exitOutput(json_encode(array("error" => $message)), array('HTTP/1.1 400 Bad Request'));
    }

    private function validateDate($date)
    {
        $dateTime = DateTime::createFromFormat('Y-m-d', $date);
        return $dateTime && $dateTime->format('Y-m-d') === $date;
    }
}