<?php
class OrderService extends BaseService
{

    private $orderModel;
    private $jsonData;

    public function __construct()
    {
        $this->orderModel = new OrderModel();
        $this->jsonData = json_decode(file_get_contents('php://input'));
    }

    // Obtain all the orders
    public function getAll()
    {
        return $this->orderModel->getAll();
    }

    
    // Obtain all the orders with the today date
    public function getToday()
    {
        return $this->orderModel->getToday();
    }

    // Obtain an order with products
    public function getOne()
    {
        $id = $GLOBALS["uriId"];
        $data = $this->orderModel->getOne($id);

        if (sizeof($data) === 0){
            return [];
        }

        $products = $this->orderModel->getOneProductOrder($id);
        $data[0]["products"] = $products;

        return $data;
    }


    public function postOne()
    {
        $data = $this->obtainData();

        $products = $data["products"];

        unset($data["products"]);
        
        $inserts = $this->orderModel->postOne(...$data);
        $id = $inserts[0]["id"];


        foreach($products as $p){
            $this->orderModel->insertProductOrder($id, $p->productId, $p->quantity);
        }

        return $id;
    }


    public function updateOne()
    {
        $id = $GLOBALS["uriId"];
        $data = $this->obtainData();

        $this->orderModel->deleteProductOrder($id);

        $products = $data["products"];

        $updates = 0;
        foreach($products as $p){
            $updates += $this->orderModel->insertProductOrder($id, $p->productId, $p->quantity);
        }

        unset($data["products"]);
        $updates += $this->orderModel->updateOne($id, ...$data);

        return $updates;
    }

    // This method deletes an order
    public function deleteOne()
    {
        $id = $GLOBALS["uriId"];
        return $this->orderModel->deleteOne($id);
    }

    // This method change the state of an order
    public function changeState()
    {
        $id = $GLOBALS["uriId"];


        $state = isset($this->jsonData->state) ? $this->jsonData->state : $this->exitOutput(json_encode(array("error" => "Missing state.")), array('HTTP/1.1 400 Bad Request'));
        
        if (!is_numeric($state)) {
            $this->exitOnError("State must be a number");
        }

        if ($state < 0 || $state > 2) {
            $this->exitOnError("State must be a number between 0 and 2");
        }
       
       return $this->orderModel->updateState($id, $state);
    }

    // Return an array with validated data.
    private function obtainData()
    {
        $orderNumber = isset($this->jsonData->orderNumber) ? $this->jsonData->orderNumber : $this->exitOnError("Missing order number.");
        $paymentMethod = isset($this->jsonData->paymentMethod) ? $this->jsonData->paymentMethod : $this->exitOnError("Missing payment method.");
        $emballageCost = isset($this->jsonData->emballageCost) ? $this->jsonData->emballageCost : $this->exitOnError("Missing emballage cost.");
        $shippingCost = isset($this->jsonData->shippingCost) ? $this->jsonData->shippingCost : $this->exitOnError("Missing shipping cost.");
        $total = isset($this->jsonData->total) ? $this->jsonData->total : $this->exitOnError("Missing total.");
        $clientId = isset($this->jsonData->clientId) ? $this->jsonData->clientId : $this->exitOnError("Missing client id.");
        $registerDate = isset($this->jsonData->registerDate) ? $this->jsonData->registerDate : $this->exitOnError("Missing register date.");
        $products = isset($this->jsonData->products) ? $this->jsonData->products : $this->exitOnError("Missing products.");

        $data = array("orderNumber" => $orderNumber, "paymentMethod" => $paymentMethod, "emballageCost" => $emballageCost, "shippingCost" => $shippingCost, "total" => $total, "clientId" => $clientId, "registerDate" => $registerDate, "products" => $products);

        $this->validateData($data);

        return $data;
    }

    // Validate the data provided in the array
    private function validateData($data)
    {

        // Type validations
        if (!is_numeric($data["orderNumber"])) {
            $this->exitOnError("Order number must be a number");
        }

        if (!is_numeric($data["paymentMethod"])) {
            $this->exitOnError("Payment method must be a number");
        }

        if (!is_numeric($data["emballageCost"])) {
            $this->exitOnError("Emballage cost must be a number");
        }

        if (!is_numeric($data["shippingCost"])) {
            $this->exitOnError("Shipping cost date must be a number");
        }

        if (!is_numeric($data["total"])) {
            $this->exitOnError("Total must be a number");
        }

        if (!is_numeric($data["clientId"])) {
            $this->exitOnError("Client Id must be a number");
        }

        if (!is_string($data["registerDate"])) {
            $this->exitOnError("registerDate must be a string");
        }

        if (!is_array($data["products"])) {
            $this->exitOnError("Products must be an array");
        }


        // Other validations
        if ($data["orderNumber"] < 1 || $data["orderNumber"] > 100 )  {
            $this->exitOnError("Order number must be a number between 1 and 100 ");
        }

        if ($data["paymentMethod"] < 1 || $data["paymentMethod"] > 4 )  {
            $this->exitOnError("Payment method must be a number between 1 and 4");
        }

        if ($data["emballageCost"] < 1 || $data["emballageCost"] > 10000 )  {
            $this->exitOnError("Emballage cost must be a number between 1 and 10000");
        }

        if ($data["shippingCost"] < 1 || $data["shippingCost"] > 20000 )  {
            $this->exitOnError("Shipping cost date must be a number between 1 and 20000");
        }

        if ($data["total"] < 1 || $data["total"] > 200000 )  {
            $this->exitOnError("Total must be a number between 1 and 200000");
        }

        if ($data["clientId"] < 1 || $data["clientId"] > 100000 )  {
            $this->exitOnError("Client Id must be a number between 1 and 100000");
        }

        if (!$this->validateDate($data["registerDate"])) {
            $this->exitOnError("Register date must be valid");
        }

        if (sizeof($data["products"]) == 0) {
            $this->exitOnError("Products can't be empty");
        }

        if (!$this->validateClient($data["clientId"])){
            $this->exitOnError("The client not exists");  
        }

        foreach($data["products"] as $p){

            if (!$this->validateProduct($p->productId)){
                $this->exitOnError("At least one of the products not exists");  
            }
    
            if (!isset($p->quantity)){
                $this->exitOnError("All product must have a quantity");                
            }

            if (!isset($p->productId)){
                $this->exitOnError("All products must have an id");                
            }


            if(!is_numeric($p->quantity)){
                $this->exitOnError("All product quantities must be numeric");
            }

            if(!is_numeric($p->productId)){
                $this->exitOnError("All product id's must be numeric");
            }

            if($p->quantity < 1 || $p->quantity > 1000){
                $this->exitOnError("All product quantities must be between 1 and 1000 ");
            }

            if($p->productId < 1 || $p->productId > 100000){
                $this->exitOnError("All product id's must be between 1 and 100000");
            }
        }

    }

    private function exitOnError($message){
        $this->exitOutput(json_encode(array("error" => $message)), array('HTTP/1.1 400 Bad Request'));
    }

    private function validateDate($date)
    {
        $dateTime = DateTime::createFromFormat('Y-m-d h:i:s', $date);
        return $dateTime && $dateTime->format('Y-m-d h:i:s') === $date;
    }

    private function validateClient($client)
    {
        $clientModel = new ClientModel();
        return sizeof($clientModel->getOne($client)) > 0;
    }

    private function validateProduct($product)
    {
        $productModel = new ProductModel();
        return sizeof($productModel->getOne($product)) > 0;

    }
}