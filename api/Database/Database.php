<?php
class Database
{
    protected $connection = null;

    protected $targetDB = "local";

    private $host; 
    private $user;
    private $pass;
    private $data;

    public function __construct()
    {
        $this->setConnectionData();

        try {

            if($this->targetDB == "local"){
                $con = mysqli_init();
                mysqli_real_connect($con, $this->host, $this->user, $this->pass, $this->data, 3306);
            }else{
                $certPath = PROJECT_ROOT_PATH . "/Core/DigiCertGlobalRootCA.crt.pem";
                $con = mysqli_init();
                mysqli_ssl_set($con,NULL,NULL, $certPath, NULL, NULL);
                mysqli_real_connect($con, $this->host, $this->user, $this->pass, $this->data, 3306, MYSQLI_CLIENT_SSL);
            }

            $this->connection = $con;
    	
            if ( mysqli_connect_errno()) {
                throw new Exception("Could not connect to database.");   
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());   
        }
    }

    private function setConnectionData(){
       $rawData = file_get_contents( dirname(__FILE__) . "/" . "config");
       $data = json_decode($rawData, true)[$this->targetDB];

       $this->host = $data["host"];
       $this->user = $data["user"];
       $this->pass = $data["pass"];
       $this->data = $data["data"];
    }

    public function executeStatement($query = "" , $params = [])
    {
        try {
            $stmt = $this->connection->prepare( $query );
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
            if( $params ) {
                $stmt->bind_param(...$params);
            }
            $stmt->execute();
            return $stmt;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }	
    }

    public function getConnection()
    {
        return $this->connection;    
    }
}
