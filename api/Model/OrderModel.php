<?php

class OrderModel extends BaseModel
{
    private $GET_ALL = "CALL sp_orders_getAll()";
    private $GET_TODAY = "CALL sp_orders_getToday()";

    private $GET_ONE = "CALL sp_orders_getOne(?)";
    private $DELETE_ONE = "CALL sp_orders_deleteOne(?)";
    private $INSERT_ONE = "CALL sp_orders_insertOne(?,?,?,?,?,?,?)";
    private $UPDATE_ONE = "CALL sp_orders_updateOne(?,?,?,?,?,?,?,?)";

    private $UPDATE_STATE = "CALL sp_orders_updateState(?,?)";
    private $GET_ONE_PRODUCT_ORDER = "CALL sp_products_order_getOne(?)";
    private $INSERT_PRODUCT_ORDER = "CALL sp_orders_product_insertOne(?,?,?)";
    private $DELETE_PRODUCT_ORDER = "CALL sp_products_order_DeleteAll(?)";



    public function getAll()
    {
        return $this->select($this->GET_ALL);
    }

    public function getToday(){
        return $this->select($this->GET_TODAY);
    }

    public function getOne($id)
    {
        return $this->select($this->GET_ONE, ["i", $id]);
    }

    public function postOne($orderNumber, $registerDate, $paymentMethod, $emballageCost, $shippingCost, $total, $clientId)
    {
        $result = $this->select($this->INSERT_ONE, ["isiiiii", $orderNumber, $registerDate, $paymentMethod, $emballageCost, $shippingCost, $total, $clientId]);
        return $result;
    }

    public function updateOne($id, $orderNumber, $registerDate, $paymentMethod, $emballageCost, $shippingCost, $total, $clientId)
    {
        $result = $this->update($this->UPDATE_ONE, ["iisiiiii", $id, $orderNumber, $registerDate, $paymentMethod, $emballageCost, $shippingCost, $total, $clientId]);
        return $result;
    }

    public function deleteOne($id)
    {
        return $this->update($this->DELETE_ONE, ["i", $id]);
    }




    public function updateState($id, $state)
    {
        $result = $this->update($this->UPDATE_STATE, ["ii", $id, $state]);
        return $result;
    }

    public function getOneProductOrder($id)
    {
        return $this->select($this->GET_ONE_PRODUCT_ORDER, ["i", $id]);
    }

    public function insertProductOrder($id, $productId, $quantity )
    {
        $result = $this->update($this->INSERT_PRODUCT_ORDER, ["iii", $id, $productId, $quantity]);
        return $result;
    }

    public function deleteProductOrder($id)
    {
        return $this->update($this->DELETE_PRODUCT_ORDER, ["i", $id]);
    }
}

