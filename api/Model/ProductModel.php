<?php

class ProductModel extends BaseModel
{
    private $GET_ALL = "CALL sp_products_getAll()";
    private $GET_ALL_IMAGELESS = "CALL sp_products_getAll_Imageless()";
    private $GET_ONE = "CALL sp_products_getOne(?)";
    private $UPDATE_ONE = "CALL sp_products_updateOne(?,?,?,?,?,?,?,?)";
    private $INSERT_ONE = "CALL sp_products_insertOne(?,?,?,?,?,?,?)";
    private $DELETE_ONE = "CALL sp_products_deleteOne(?)";
    private $GET_IMAGE = "CALL sp_product_getImage(?)";
    
    public function getAll()
    {
        return $this->select($this->GET_ALL);
    }

    public function getAllImageLess()
    {
        return $this->select($this->GET_ALL_IMAGELESS);
    }

    public function getOne($id)
    {
        return $this->select($this->GET_ONE, ["i", $id]);
    }

    public function postOne($name, $description, $price, $category, $ingredients, $image, $discount)
    {
        $result = $this->update($this->INSERT_ONE, ["ssisssi", $name, $description, $price, $category, $ingredients, $image, $discount]);
        return $result;
    }

    public function updateOne($id, $name, $description, $price, $category, $ingredients, $image, $discount)
    {
        $result = $this->update($this->UPDATE_ONE, ["ississsi", $id, $name, $description, $price, $category, $ingredients, $image, $discount]);
        return $result;
    }

    public function deleteOne($id)
    {
        return $this->update($this->DELETE_ONE, ["i", $id]);
    }

    public function getImage($id)
    {
        return $this->select($this->GET_IMAGE, ["i", $id]);
    }
}

