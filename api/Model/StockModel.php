<?php

class StockModel extends BaseModel
{
    private $GET_ALL = "CALL sp_stocks_getAll()";
    private $GET_ONE = "CALL sp_stocks_getOne(?)";
    private $UPDATE_ONE = "CALL sp_stocks_updateOne(?,?,?,?,?)";
    private $INSERT_ONE = "CALL sp_stocks_insertOne(?,?,?,?)";
    private $DELETE_ONE = "CALL sp_stocks_deleteOne(?)";
    private $DEPLETE_ONE = "CALL sp_stocks_depleteOne(?)";

    public function getAll()
    {
        return $this->select($this->GET_ALL);
    }

    public function getOne($id)
    {
        return $this->select($this->GET_ONE, ["i", $id]);
    }

    public function postOne($name, $quantity, $price, $registerDate)
    {
        $result = $this->update($this->INSERT_ONE, ["siis", $name, $quantity, $price, $registerDate]);
        return $result;
    }

    public function updateOne($id, $name, $quantity, $price, $registerDate)
    {
        $result = $this->update($this->UPDATE_ONE, ["isiis", $id, $name, $quantity, $price, $registerDate]);
        return $result;
    }

    public function deleteOne($id)
    {
        return $this->update($this->DELETE_ONE, ["i", $id]);
    }

    public function depleteOne($id)
    {
        return $this->update($this->DEPLETE_ONE, ["i", $id]);
    }
}

