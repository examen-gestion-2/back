<?php

class UserModel extends BaseModel
{
    private $GET_ONE = "CALL sp_users_getOne(?)";
    private $UPDATE_ONE = "CALL sp_users_updateOne(?,?,?,?)";
    private $VERIFY_ONE = "CALL sp_users_verifyOne(?,?)";

    public function getOne($id)
    {
        return $this->select($this->GET_ONE, ["i", $id]);
    }

    public function updateOne($id, $userName, $password, $rol)
    {
        $result = $this->update($this->UPDATE_ONE, ["issi", $id, $userName, $password, $rol]);
        return $result;
    }

    public function verifyOne($userName, $password)
    {
        $result = $this->select($this->VERIFY_ONE,["ss", $userName, $password]);
        return $result;
    }
}

