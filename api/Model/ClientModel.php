<?php

class ClientModel extends BaseModel
{
    private $GET_ALL = "CALL sp_clients_getAll()";
    private $GET_ONE = "CALL sp_clients_getOne(?)";
    private $UPDATE_ONE = "CALL sp_clients_updateOne(?,?,?,?,?)";
    private $INSERT_ONE = "CALL sp_clients_insertOne(?,?,?,?)";
    private $DELETE_ONE = "CALL sp_clients_deleteOne(?)";

    public function getAll()
    {
        return $this->select($this->GET_ALL);
    }

    public function getOne($id)
    {
        return $this->select($this->GET_ONE, ["i", $id]);
    }

    public function postOne($name, $phone, $address, $registerDate)
    {
        $result = $this->update($this->INSERT_ONE, ["ssss", $name, $phone, $address, $registerDate]);
        return $result;
    }

    public function updateOne($id, $name, $phone, $address, $registerDate)
    {
        $result = $this->update($this->UPDATE_ONE, ["issss", $id, $name, $phone, $address, $registerDate]);
        return $result;
    }


    public function deleteOne($id)
    {
        return $this->update($this->DELETE_ONE, ["i", $id]);
    }
}

